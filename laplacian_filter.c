#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "var.h"
#include "bmpfile.h"

#define SIZE

// laplacian filter処理する関数
void laplacian(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
             Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH]);

int main(int argc, char *argv[])
{
  imgdata idata;

  if (argc < 3) {
    printf("使用法：cpbmp コピー元.bmp コピー先.bmp\n");
  }

  else {
    if (readBMPfile(argv[1], &idata) > 0){
      printf("指定コピー元ファイル%sが見つかりません\n",argv[1]);
    }
    else {
      // laplacian filter処理
      laplacian(idata.source, idata.results);
    }
    if (writeBMPfile(argv[2], &idata) > 0){
      printf("コピー先ファイル%sに保存できませんでした\n",argv[2]);
    }
  }
}

void laplacian(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
             Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH])
{
  char delta[3][3] = {{ 0,  1,  0},
                      { 1, -4,  1},
                      { 0,  1,  0}};

  int    x, y, i, j; // for文用の変数
  int    color;
  double sum;        // 近傍画素の合計値
  double slope;      // 勾配

  for (color = 0; color < 3; color++) {
    for (y = 0; y < SIZE; y++) {
      for (x = 0; x < SIZE; x++) {
        slope = 0;
        sum  = 0;
        // 空間フィルタ処理
        for (i = -1; i < 2; i++) {
          for (j = -1; j < 2; j++) {
            // 画像の端だったらスキップ
            if (y+i < SIZE && y+i >=0 && x+j < SIZE && x+j >=0 ){
              sum += delta[i+1][j+1] * source[color][y+i][x+j];
            }
          }
        }
        // 勾配の計算
        slope = sum + 128;
        if(slope > (SIZE-1)) slope = SIZE-1;
        if(slope < 0) slope = 0;
        // 画像に格納
        results[color][y][x] = slope;
      }
    }
  }
}
