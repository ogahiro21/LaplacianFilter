# Laplacian Filter

# About Laplacian filter
画像の微分を行い特徴を抽出するフィルタ。  
$`\nabla^{2} f = \frac{\partial^2 f}{\partial^2 x} + \frac{\partial^2 f}{\partial^2 y}`$  
$`\nabla^{2} f = f[i-1,j] + f[i+1,j] + f[i,j-1] + f[i,j+1] -4f[i,f] `$  
- 入力画像  
![in](https://gitlab.com/ogahiro21/LaplacianFilter/raw/image/image/in13.jpeg)

- 出力画像  
![out](https://gitlab.com/ogahiro21/LaplacianFilter/raw/image/image/out13.jpeg)  

## Usage
**コンパイル**
```
gcc -o cpbmp laplacian_filter.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in13.bmp ans13.bmp
```
